import { TezosToolkit } from '@taquito/taquito';
import { InMemorySigner } from '@taquito/signer';
import { Schema } from '@taquito/michelson-encoder';
import { Parser, Expr, packDataBytes, packData} from '@taquito/michel-codec';
import { buf2hex, hex2buf } from '@taquito/utils';
import { blake2b } from 'blakejs';

// ChainIDs
// Used to avoid replay attack
const CHAIN_ID_MAINNET = 'NetXdQprcVkpaWU';
const CHAIN_ID_GHOSTNET = 'NetXnHfVqm9iesp';

// Type of the parameters of the transfer_gasless entrypoint
const TRANSFER_PARAM_TYPE = "(list (pair (address %from_) (list %txs (pair (address %to_) (nat %token_id) (nat %amount)))))";
const PAYLOAD_TO_SIGN_TYPE = '(pair (pair address chain_id) (pair nat bytes))';

// Initialize the parser
const parser = new Parser();

function getTransferHash(transferParams: any) {
  // Generate JSON representation of the transfer parameters
  const transferType = parser.parseMichelineExpression(TRANSFER_PARAM_TYPE);

  // Get the schema of the transfer parameters
  const transferSchema = new Schema(transferType as any);

  // Encode the transfer parameters
  const transferData = transferSchema.Encode(transferParams);

  // Pack the transfer parameters to bytes
  const transferBytes = packDataBytes(
    transferData as any,
    transferType as any
  );

  // Hash the transfer parameters
  const transferHash = buf2hex(blake2b(hex2buf(transferBytes.bytes), null as any, 32) as Buffer);

  return transferHash;
}

function makePayload(transferHash: string) {
  const contractAddress = 'KT1Rrv2usJnSVfNFmJot4n2aRqff1K331sts';   // The address of the permit contract
  const chainId = CHAIN_ID_GHOSTNET;  // The chain id of the network
  const permitCounter = 2;    // Need to be retrieve in the permit contract

  const payloadToSignType = parser.parseMichelineExpression(PAYLOAD_TO_SIGN_TYPE);
  const payloadToSignData = parser.parseMichelineExpression(`(Pair (Pair "${contractAddress}" "${chainId}") (Pair ${permitCounter} 0x${transferHash}))`);

  const payloadToSign = packDataBytes(
    payloadToSignData as any,
    payloadToSignType as any
  );

  return payloadToSign.bytes;
}

async function transfer_gassless(from: string, to: string , amount: number) {
  try {
    // Initialize the signer that calls the transfer_gasless entrypoint
    const senderPrivateKey = ""; // The private key of the account that injects the operation
    const senderTezosToolkit = new TezosToolkit('https://ghostnet.ecadinfra.com');
    senderTezosToolkit.setProvider({ signer: await InMemorySigner.fromSecretKey(senderPrivateKey) });

    // Initialize the signer that signs the payload
    const ownerPrivateKey = ""; // The private key of the account that owns the token
    const ownerTzToolkit = new TezosToolkit('https://ghostnet.ecadinfra.com');
    ownerTzToolkit.setProvider({ signer: await InMemorySigner.fromSecretKey(ownerPrivateKey) });

    // Adderss of the FA2 contract
    const smartContract = "KT1Qob3bewFsv3LwJu97aAfEWyoZcEtkhTVe";
    
    // Build the transfer arguments
    const transferParams = [
      {
        from_: from,
        txs: [
          {
              to_: to,
              token_id: 0,
              amount: amount
          }
        ]
      }
    ]

    const transferHash = getTransferHash(transferParams);
    const payload = makePayload(transferHash);

    const gasless_param =  {
      transfer_params : transferParams,
      user_pk         : await ownerTzToolkit.signer.publicKey(),
      user_sig        : (await ownerTzToolkit.signer.sign(payload)).prefixSig //<-- Pass the prefixSig to the contract
    }

    const fa2_contract = await senderTezosToolkit.contract.at(smartContract);
    const tx = await fa2_contract.methods.transfer_gasless([ gasless_param ]).send();
    await tx.confirmation();

    console.log(`Operation injected: https://ghostnet.tzkt.io/${tx.hash}`);
  } catch (err) {
    console.log(`Error: ${err}`);
  }
}

transfer_gassless(
  'tz1aAvMu1sNAGwDNahHcQc4yDZ7WwoDNjFzu',
  'tz1PbQP4AYHFSNL5pt41GyZed6aUWBnpd3SS',
  1
);
